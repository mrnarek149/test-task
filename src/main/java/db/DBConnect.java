package db;

import org.hsqldb.jdbc.JDBCPool;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Класс подключения к БД, использующий шаблон Singleton
 */
public final class DBConnect {

    private static DataSource dataSource;
    /** Адрес, где нахоидтся файл с данными к БД */
    private static final String db_account_directory = "hospital_db/db_account.properties";
    private static boolean key = false;

    private DBConnect() { }

    public static DBConnect getInstance(){
        if (!key) {
            try (FileInputStream fis = new FileInputStream(db_account_directory);) {
                Properties properties = new Properties();
                properties.load(fis);
                JDBCPool pool = new JDBCPool(Integer.parseInt(properties.getProperty("service.maxPoolSize")));
                pool.setURL(properties.getProperty("service.url"));
                pool.setUser(properties.getProperty("service.user"));
                pool.setPassword(properties.getProperty("service.password"));
                dataSource = pool;
                key = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return Singleton.Instance;
    }

    private static final class Singleton{
        private static DBConnect Instance = new DBConnect();
    }

    public DataSource getDataSource(){ return dataSource;}
}
