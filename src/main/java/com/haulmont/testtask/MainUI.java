package com.haulmont.testtask;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import views.MainView;
import views.ShowDoctorsView;
import views.ShowPatientsView;
import views.ShowRecipesView;

@Theme(ValoTheme.THEME_NAME)
public class MainUI extends UI {
    @Override
    protected void init(VaadinRequest request) {
        VerticalLayout main = new VerticalLayout();

        Navigator navigator = new Navigator(this, main);
        navigator.addView("", MainView.class);
        navigator.addView("patients", ShowPatientsView.class);
        navigator.addView("doctors", ShowDoctorsView.class);
        navigator.addView("recipes", ShowRecipesView.class);
        setContent(main);
    }
}