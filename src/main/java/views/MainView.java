package views;

import com.vaadin.navigator.View;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class MainView extends VerticalLayout implements View{
    public MainView(){
        Label label = new Label("<h1>Главное меню</h1", ContentMode.HTML);
        Button buttonShowPatients = new Button("Отображение пациентов", clickEvent -> getUI().getNavigator().navigateTo("patients"));
        Button buttonShowDoctors = new Button("Отображение докторов", clickEvent -> getUI().getNavigator().navigateTo("doctors"));
        Button buttonShowRecipes = new Button("Отображение врачебных рецептов", clickEvent -> getUI().getNavigator().navigateTo("recipes"));

        addComponents(label, buttonShowDoctors, buttonShowPatients, buttonShowRecipes);
    }
}
