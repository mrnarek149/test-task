package views;

import com.vaadin.data.Binder;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import entities.Doctor;
import entities.Patient;
import entities.Priority;
import entities.Recipe;
import modalWindow.EditRecipe;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import java.util.Set;

public class ShowRecipesView extends ShowViews<Recipe> implements View {

    private List<Patient> patientList = patientService.getAll();
    private List<Doctor> doctorList = doctorService.getAll();
    /** Элементы для заполнения таблицы*/
    private ListDataProvider<Recipe> recipeListDataProvider = DataProvider.ofCollection(recipeService.getAll());

    Grid<Recipe> grid = CreateGrid(new Grid<>(Recipe.class), recipeListDataProvider);

    public ShowRecipesView() {
        Label title = new Label("<h1>Рецепты<h1>", ContentMode.HTML);

        VerticalLayout filter = CreateFilter();
        HorizontalLayout mainLayout = new HorizontalLayout(grid, filter);

        ButtonsListener();

        addComponents(title, mainLayout, buttons);
    }

    @Override
    protected Grid<Recipe> CreateGrid(Grid<Recipe> grid, ListDataProvider<Recipe> recipeListDataProvider) {
        grid.setDataProvider(recipeListDataProvider);
        grid.setWidth("1300px");
        grid.setHeight("500px");
        grid.getColumn("id").setCaption("ID");
        grid.addComponentColumn(recipe -> {
            TextArea area = new TextArea();
            area.setValue(recipe.getDescription());
            area.setReadOnly(true);
            area.setSizeFull();
            area.setHeight("70px");
            return area;
        }).setId("descr").setCaption("Описание");
        grid.getColumn("patient_id").setCaption("ID Пациента");
        grid.getColumn("doctor_id").setCaption("ID Доктора");
        grid.addColumn(recipe -> recipe.getDateCreated().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM))).setId("date").setCaption("Дата создания");
        grid.addColumn(recipe -> recipe.getValidity().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM))).setId("valid").setCaption("Срок действия");
        grid.getColumn("priority").setCaption("Приоритет");
        grid.addComponentColumn(recipe -> {
            Button button = new Button("Изменить");
            button.setSizeFull();
            button.addClickListener(changeEvent -> getUI().addWindow(new EditRecipe(recipe, grid)));
            return button;
        }).setCaption("Изменить").setId("change");
        grid.setColumns("id", "descr", "patient_id", "doctor_id", "date", "valid", "priority", "change");
        grid.setRowHeight(70);
        grid.setSelectionMode(Grid.SelectionMode.MULTI);

        return grid;
    }

    @Override
    protected void ButtonsListener() {
        addButton.addClickListener(addClick -> {
            Binder<Recipe> binder = new Binder<>(Recipe.class);

            errorLabel.setVisible(false);

            TextArea description = new TextArea();
            description.setHeight("70");
            description.setCaption("Описание");

            ComboBox<Patient> idPatient = new ComboBox<>();
            idPatient.setItems(patientList);
            idPatient.setEmptySelectionAllowed(false);
            idPatient.setPopupWidth("400px");
            idPatient.setItemCaptionGenerator(Patient::toString);
            idPatient.setCaption("Выберите пациента");

            ComboBox<Doctor> idDoctor = new ComboBox<>();
            idDoctor.setItems(doctorList);
            idDoctor.setEmptySelectionAllowed(false);
            idDoctor.setPopupWidth("400px");
            idDoctor.setItemCaptionGenerator(Doctor::toString);
            idDoctor.setCaption("Выберите доктора");

            DateTimeField dataCreated = new DateTimeField();
            dataCreated.setCaption("Дата создания");

            DateTimeField validity = new DateTimeField();
            validity.setCaption("Срок действия");

            ComboBox<Priority> priorityComboBox = new ComboBox<>();
            priorityComboBox.setItems(Priority.values());
            priorityComboBox.setEmptySelectionAllowed(false);
            priorityComboBox.setCaption("Приоритет");

            binder.forField(idDoctor).asRequired("Выберите доктора").bind(Recipe::getDoctor, Recipe::setDoctor);
            binder.forField(idPatient).asRequired("Выберите пациента").bind(Recipe::getPatient, Recipe::setPatient);
            binder.forField(description).withValidator(name -> name.length() < 5001, "Ограничения: не более 5000 букв").bind(Recipe::getDescription, Recipe::setDescription);
            binder.forField(dataCreated).asRequired("Укажите дату").bind(Recipe::getDateCreated, Recipe::setDateCreated);
            binder.forField(validity).asRequired("Укажите дату").bind(Recipe::getValidity, Recipe::setValidity);
            binder.forField(priorityComboBox).asRequired("Выберите приоритет").bind(Recipe::getPriority, Recipe::setPriority);

            /** Кнопки принятия и отмены добавления элементов */
            Button buttonAccept = new Button(VaadinIcons.CHECK);
            Button buttonCancel = new Button(VaadinIcons.CLOSE);
            HorizontalLayout buttonRecipe = new HorizontalLayout(buttonAccept, buttonCancel, errorLabel);
            HorizontalLayout addLayout = new HorizontalLayout(idPatient, idDoctor, dataCreated, validity, priorityComboBox, description, buttonRecipe);

            buttonAccept.addClickListener(clickEvent -> {
                if (binder.isValid()) {
                    Recipe recipe = new Recipe(description.getValue(),
                            idPatient.getValue().getId(),
                            idDoctor.getValue().getId(),
                            dataCreated.getValue(),
                            validity.getValue(),
                            priorityComboBox.getValue().name());
                    if (recipeService.save(recipe)) {           // Попытка сохранить новый элемент
                        recipeListDataProvider.getItems().add(recipe);
                        grid.getDataProvider().refreshAll();
                        removeComponent(addLayout);
                    } else {
                        errorLabel.setVisible(true);
                        errorLabel.setValue("Ошибка записи в БД");
                        buttonAccept.setStyleName(ValoTheme.BUTTON_DANGER);
                    }
                } else {
                    buttonAccept.setStyleName(ValoTheme.BUTTON_DANGER);
                }
            });
            buttonCancel.addClickListener(clickEvent -> removeComponent(addLayout));
            addLayout.setComponentAlignment(buttonRecipe, Alignment.MIDDLE_CENTER);
            addComponent(addLayout);
        });

        deleteButton.addClickListener(clickEvent -> {
            Set<Recipe> recipes = grid.getSelectedItems();
            for (Recipe recipe : recipes) {
                recipeListDataProvider.getItems().remove(recipe);
                recipeService.delete(recipe.getId());
            }
            grid.getDataProvider().refreshAll();
        });

        buttons = new HorizontalLayout(addButton, deleteButton);
    }

    /**
     * Фильтр таблицы
     * */
    private VerticalLayout CreateFilter() {
        Label titleFilter = new Label("<h2>Фильтр</h2>", ContentMode.HTML);

        TextArea descriptionFilter = new TextArea();
        descriptionFilter.setWidth("300px");
        descriptionFilter.setHeight("70px");
        descriptionFilter.setCaption("Поиск по описанию");

        ComboBox<Patient> patientFilter = new ComboBox<>();
        patientFilter.setEmptySelectionAllowed(false);
        patientFilter.setWidth("300px");
        patientFilter.setPopupWidth("400px");
        patientFilter.setItems(patientList);
        patientFilter.setItemCaptionGenerator(Patient::toString);
        patientFilter.setCaption("Выберите пациента");

        ComboBox<Priority> priorityFilter = new ComboBox<>();
        priorityFilter.setWidth("300px");
        priorityFilter.setItems(Priority.values());
        priorityFilter.setEmptySelectionAllowed(false);
        priorityFilter.setCaption("Выберите приоритет");

        Button acceptFilter = new Button("Применить");
        Button cancelFilter = new Button("Отменить");

        acceptFilter.addClickListener(clickEvent -> {
            ListDataProvider<Recipe> filterList = new ListDataProvider<>(recipeListDataProvider.getItems());
            /** Если форма заполнена, то добавляем фильтр */
            if (!patientFilter.isEmpty()) {
                filterList.addFilter(setfilter -> setfilter.getPatient_id() == patientFilter.getValue().getId());
            }
            if (!priorityFilter.isEmpty()) {
                filterList.addFilter(setfilter -> setfilter.getPriority() == priorityFilter.getValue());
            }
            if (!descriptionFilter.isEmpty()) {
                filterList.addFilter(setfilter -> setfilter.getDescription().contains(descriptionFilter.getValue()));
            }
            grid.setDataProvider(filterList);
        });

        cancelFilter.addClickListener(clickEvent -> {
            grid.setDataProvider(recipeListDataProvider);
            patientFilter.setValue(patientFilter.getEmptyValue());
            priorityFilter.setValue(priorityFilter.getEmptyValue());
            descriptionFilter.setValue(descriptionFilter.getEmptyValue());
        });
        HorizontalLayout buttonsFilter = new HorizontalLayout(acceptFilter, cancelFilter);

        VerticalLayout filter = new VerticalLayout(titleFilter, patientFilter, priorityFilter, descriptionFilter, buttonsFilter);
        filter.setComponentAlignment(titleFilter, Alignment.MIDDLE_CENTER);
        return filter;
    }
}
