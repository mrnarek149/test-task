package views;

import com.vaadin.data.Binder;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import entities.Doctor;
import modalWindow.EditDoctor;
import service.RecipeService;

import java.util.Set;

public class ShowDoctorsView extends ShowViews<Doctor> implements View {

    private ListDataProvider<Doctor> doctorListDataProvider = DataProvider.ofCollection(doctorService.getAll());
    private Grid<Doctor> grid = CreateGrid(new Grid<>(Doctor.class), doctorListDataProvider);

    public ShowDoctorsView() {
        Label title = new Label("<h1>Доктора</h1>", ContentMode.HTML);
        errorLabel.setVisible(false);

        ButtonsListener();

        addComponents(title, grid, buttons, errorLabel);
    }

    @Override
    protected Grid<Doctor> CreateGrid(Grid<Doctor> grid, ListDataProvider<Doctor> listDataProvider) {
        grid.setDataProvider(doctorListDataProvider);
        grid.setWidth("1100px");
        grid.setColumnOrder("id", "lastName", "firstName", "middleName", "specialization");
        grid.getColumn("id").setCaption("ID");
        grid.getColumn("lastName").setCaption("Фамилия");
        grid.getColumn("firstName").setCaption("Имя");
        grid.getColumn("middleName").setCaption("Отчество");
        grid.getColumn("specialization").setCaption("Специальность");
        grid.addComponentColumn(doctor -> {
            Button button = new Button("Изменить");
            button.addClickListener(changeEvent -> getUI().addWindow(new EditDoctor(doctor, grid)));
            return button;
        }).setCaption("Изменить").setId("change");
        grid.setRowHeight(43);
        grid.setSelectionMode(Grid.SelectionMode.MULTI);

        return grid;
    }

    @Override
    protected void ButtonsListener() {
        deleteButton.addClickListener(clickEvent -> {
            Set<Doctor> set = grid.getSelectedItems();
            for (Doctor doctor : set) {
                if (recipeService.findByDoctor(doctor.getId()).size() > 0) {        // Проверка на существование рецептов у доктора
                    if (!errorLabel.isVisible()) {
                        errorLabel.setVisible(true);
                    }
                    errorLabel.setValue(errorLabel.getValue() + "\n Необходимо удалить рецепты доктора с ID: " + doctor.getId());
                } else {
                    doctorListDataProvider.getItems().remove(doctor);
                    doctorService.delete(doctor.getId());
                    if (errorLabel.isVisible()) {
                        errorLabel.setValue("Ошибка удаления: ");
                        errorLabel.setVisible(false);
                    }
                }
                grid.getDataProvider().refreshAll();
            }
        });

        addButton.addClickListener(clickEvent -> {
            Binder<Doctor> binder = new Binder<>(Doctor.class);

            TextField lastName = new TextField();
            lastName.setPlaceholder("Фамилия");                                 // Проверка на содержание лишних символов
            binder.forField(lastName).withValidator(name -> (name.length() < 16) && (name.matches("[\\p{L}]+")), "Ограничения: не более 15 букв")
                    .asRequired("Введите фамилию").bind(Doctor::getLastName, Doctor::setLastName);

            TextField firstName = new TextField();
            firstName.setPlaceholder("Имя");
            binder.forField(firstName).withValidator(name -> (name.length()) < 16 && (name.matches("[\\p{L}]+")), "Ограничения: не более 15 букв")
                    .asRequired("Введите имя").bind(Doctor::getFirstName, Doctor::setFirstName);

            TextField middleName = new TextField();
            middleName.setPlaceholder("Отчество");
            binder.forField(middleName).withValidator(name -> (name.length()) < 16 && (name.matches("[\\p{L}| ]+")), "Ограничения: не более 15 букв")
                    .bind(Doctor::getMiddleName, Doctor::setMiddleName);

            TextField specialization = new TextField();
            specialization.setPlaceholder("Специальность");
            binder.forField(specialization).withValidator(name -> (name.length()) < 16 && (name.matches("[\\p{L}| ]+")), "Ограничения: не более 15 букв")
                    .asRequired("Введите специальность").bind(Doctor::getSpecialization, Doctor::setSpecialization);

            Button accept = new Button(VaadinIcons.CHECK);
            Button close = new Button(VaadinIcons.CLOSE);

            /** Размещение форм для добавления доктора */
            HorizontalLayout addLayout = new HorizontalLayout(lastName, firstName, middleName, specialization, accept, close);
            addComponents(addLayout);

            accept.addClickListener(acceptEvent -> {
                if (middleName.isEmpty()) {
                    middleName.setValue(" ");
                }
                if (binder.isValid()) {
                    Doctor doctor = new Doctor(firstName.getValue(), middleName.getValue(), lastName.getValue(), specialization.getValue());
                    if (doctorService.save(doctor)) {       // Если запись в БД прошла успешно
                        doctorListDataProvider.getItems().add(doctor);
                        grid.getDataProvider().refreshAll();
                        if (errorLabel.isVisible()) {
                            errorLabel.setVisible(false);
                            errorLabel.setValue("");
                        }
                        removeComponent(addLayout);
                    } else {
                        if (!errorLabel.isVisible()) {
                            errorLabel.setVisible(true);
                            errorLabel.setValue("Ошибка записи в БД");
                            accept.setStyleName(ValoTheme.BUTTON_DANGER);
                        }
                    }
                } else {
                    accept.setStyleName(ValoTheme.BUTTON_DANGER);     // Если пользователь ввёл неверные данные кнопка становится красной
                }
            });
            close.addClickListener(closeEvent -> removeComponent(addLayout));

        });

        Button stat = new Button("Показать статистику");
        stat.addClickListener(statEvent -> {
            if (stat.getCaption().contains("Показать")) {
                grid.addColumn(doctor -> (new RecipeService().findByDoctor(doctor.getId()).size())).setCaption("Число рецептов").setId("recipes");
                stat.setCaption("Скрыть статистику");
            } else if (stat.getCaption().contains("Скрыть")) {
                grid.removeColumn("recipes");
                stat.setCaption("Показать статистику");
            }
        });

        buttons = new HorizontalLayout(addButton, deleteButton, stat);
    }
}
