package views;

import com.vaadin.annotations.Theme;
import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToLongConverter;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import entities.Patient;
import modalWindow.EditPatient;

import java.util.Set;

@Theme(ValoTheme.THEME_NAME)
public class ShowPatientsView extends ShowViews<Patient> implements View {
    ListDataProvider<Patient> patientListDataProvider = DataProvider.ofCollection(patientService.getAll());
    Grid<Patient> grid = CreateGrid(new Grid<>(Patient.class), patientListDataProvider);

    public ShowPatientsView() {
        Label title = new Label("<h1>Пациенты</h1", ContentMode.HTML);
        errorLabel.setVisible(false);
        ButtonsListener();
        addComponents(title, grid, buttons, errorLabel);
    }

    @Override
    protected Grid<Patient> CreateGrid(Grid<Patient> grid, ListDataProvider<Patient> listDataProvider) {
        grid.setDataProvider(patientListDataProvider);
        grid.setWidth("1100px");
        grid.setColumnOrder("id", "lastName", "firstName", "middleName", "tel");
        grid.getColumn("id").setCaption("ID");
        grid.getColumn("lastName").setCaption("Фамилия");
        grid.getColumn("firstName").setCaption("Имя");
        grid.getColumn("middleName").setCaption("Отчество");
        grid.getColumn("tel").setCaption("Номер телефона");
        grid.addComponentColumn(patient -> {
            Button button = new Button("Изменить");
            button.addClickListener(changeEvent -> getUI().addWindow(new EditPatient(patient, grid)));
            return button;
        }).setCaption("Изменить").setId("change");
        grid.setRowHeight(43);
        grid.setSelectionMode(Grid.SelectionMode.MULTI);

        return grid;
    }

    @Override
    protected void ButtonsListener() {
        deleteButton.addClickListener(clickEvent -> {
            Set<Patient> set = grid.getSelectedItems();
            for (Patient patient : set) {
                if (recipeService.findByPatient(patient.getId()).size() > 0) {
                    if (!errorLabel.isVisible()) errorLabel.setVisible(true);
                    errorLabel.setValue(errorLabel.getValue() + "\n Необходимо удалить рецепты для пациента с ID: " + patient.getId());
                } else {
                    patientListDataProvider.getItems().remove(patient);
                    patientService.delete(patient.getId());
                    if (errorLabel.isVisible()) {
                        errorLabel.setValue("Ошибка удаления: ");
                        errorLabel.setVisible(false);
                    }
                }
                grid.getDataProvider().refreshAll();
            }
        });

        addButton.addClickListener(clickEvent -> {
            Binder<Patient> binder = new Binder<>(Patient.class);
            TextField lastName = new TextField();
            lastName.setPlaceholder("Фамилия");                                                                 // Проверка на содержание лишних символов
            binder.forField(lastName).asRequired("Введите фамилию").withValidator(name -> name.length() < 16 && name.matches("[\\p{L}]+"), "Ограничения: не более 15 букв")
                    .bind(Patient::getLastName, Patient::setLastName);

            TextField firstName = new TextField();
            firstName.setPlaceholder("Имя");
            binder.forField(firstName).asRequired("Введите фамилию").withValidator(name -> name.length() < 16 && name.matches("[\\p{L}]+"), "Ограничения: не более 15 букв")
                    .bind(Patient::getFirstName, Patient::setFirstName);

            TextField middleName = new TextField();
            middleName.setPlaceholder("Отчество");
            binder.forField(middleName).withValidator(name -> name.length() < 16 && name.matches("[\\p{L}| ]+"), "Ограничения: не более 15 букв")
                    .bind(Patient::getMiddleName, Patient::setMiddleName);

            TextField tel = new TextField();
            tel.setPlaceholder("Номер телефона");
            binder.forField(tel).asRequired("Введите номер").withValidator(number -> !number.contains(" "), "Содержит пробел")
                    .withValidator(number -> number.length() < 14, "Не более 13 цифр").withConverter(new StringToLongConverter("Можно вводить только цифры"))
                    .bind(Patient::getTel, Patient::setTel);

            Button accept = new Button(VaadinIcons.CHECK);
            Button close = new Button(VaadinIcons.CLOSE);
            HorizontalLayout addLayout = new HorizontalLayout(lastName, firstName, middleName, tel, accept, close);
            addComponents(addLayout);

            accept.addClickListener(acceptEvent -> {
                if (middleName.isEmpty()) middleName.setValue(" ");
                if (binder.isValid()) {
                    Patient patient = new Patient(firstName.getValue(), middleName.getValue(), lastName.getValue(), Long.parseLong(tel.getValue()));
                    if (patientService.save(patient)) { // Попытка сохранить новый элемент
                        patientListDataProvider.getItems().add(patient);
                        grid.getDataProvider().refreshAll();
                        if (errorLabel.isVisible()) {
                            errorLabel.setVisible(false);
                            errorLabel.setValue("");
                        }
                        removeComponent(addLayout);
                    } else {
                        if (!errorLabel.isVisible()) {
                            errorLabel.setVisible(true);
                            errorLabel.setValue("Ошибка записи в БД");
                            accept.setStyleName(ValoTheme.BUTTON_DANGER);
                        }
                    }
                } else {
                    accept.setStyleName(ValoTheme.BUTTON_DANGER);
                }
            });
            close.addClickListener(closeEvent -> {
                removeComponent(addLayout);
                if (errorLabel.isVisible()) {
                    errorLabel.setVisible(false);
                    errorLabel.setValue("");
                }
            });

        });

        buttons = new HorizontalLayout(addButton, deleteButton);
    }
}
