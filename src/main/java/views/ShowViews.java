package views;

import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import service.DoctorService;
import service.PatientService;
import service.RecipeService;

/**
 * Формироание страниц отображающих докторов, пациентов и рецетпы
 * @param <T> Тип отображаемой сущности
 */
public abstract class ShowViews<T> extends VerticalLayout{

    /** Переменные для работы с БД */
    protected RecipeService recipeService = new RecipeService();
    protected DoctorService doctorService = new DoctorService();
    protected PatientService patientService = new PatientService();

    /** Кнопки для добавления и удаления докторов/пациентов/рецептов */
    protected Button addButton = new Button("Добавить");
    protected Button deleteButton = new Button("Удалить");

    /** Горизонтальное отображение кнопок добавления и удаления */
    protected HorizontalLayout buttons;
    Label errorLabel = new Label("", ContentMode.PREFORMATTED);

    /**
     * Формирование таблицы
     * @param grid Таблица которую настраиваем
     * @param listDataProvider Элементы таблицы
     * @return Возвращаем сформированную таблицу
     */
    protected abstract Grid<T> CreateGrid(Grid<T> grid, ListDataProvider<T> listDataProvider);

    /**
     * Настройка слушателей для кнопок добавления, удаления и статистики
     */
    protected abstract void ButtonsListener();
}
