package modalWindow;

import com.vaadin.data.Binder;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import entities.Doctor;
import service.DoctorService;

/**
 * Формирование формы редактирования
 */
public class EditDoctor extends Edit {
    public EditDoctor(Doctor doctor, Grid grid) {
        init();
        setHeight("260px");
        setWidth("225px");
        DoctorService doctorService = new DoctorService();

        Binder<Doctor> binder = new Binder<>(Doctor.class);

        TextField lastName = new TextField();
        lastName.setValue(doctor.getLastName());
        lastName.setPlaceholder("Фамилия");

        TextField firstName = new TextField();
        firstName.setValue(doctor.getFirstName());
        firstName.setPlaceholder("Имя");

        TextField middleName = new TextField();
        middleName.setValue(doctor.getMiddleName());
        middleName.setPlaceholder("Отчество");

        TextField specialization = new TextField();
        specialization.setValue(doctor.getSpecialization());
        specialization.setPlaceholder("Специальность");

        binder.forField(lastName).asRequired("Введите фамилию").withValidator(name -> (name.length() < 16) && (name.matches("[\\p{L}]+")), "Ограничения: не более 15 букв")
                .bind(Doctor::getLastName, Doctor::setLastName);
        binder.forField(firstName).asRequired("Введите имя").withValidator(name -> (name.length() < 16) && (name.matches("[\\p{L}]+")), "Ограничения: не более 15 букв")
                .bind(Doctor::getFirstName, Doctor::setFirstName);
        binder.forField(middleName).withValidator(name -> (name.length() < 16) && (name.matches("[\\p{L}| ]+")), "Ограничения: не более 15 букв")
                .bind(Doctor::getMiddleName, Doctor::setMiddleName);
        binder.forField(specialization).asRequired("Введите специальность").withValidator(name -> (name.length() < 16) && (name.matches("[\\p{L}| ]+")), "Ограничения: не более 15 букв")
                .bind(Doctor::getSpecialization, Doctor::setSpecialization);

        Button buttonAccept = new Button("ОК");
        buttonAccept.addClickListener(clickEvent -> {
            if (middleName.isEmpty()) {
                middleName.setValue(" ");
            }
            if (binder.isValid()) {
                doctor.setLastName(lastName.getValue());
                doctor.setFirstName(firstName.getValue());
                doctor.setMiddleName(middleName.getValue());
                doctor.setSpecialization(specialization.getValue());
                doctorService.update(doctor, doctor.getId());
                grid.getDataProvider().refreshAll();
                close();
            } else {
                buttonAccept.setStyleName(ValoTheme.BUTTON_DANGER);
            }
        });
        Button buttonCancel = new Button("Отменить");
        buttonCancel.addClickListener(clickEvent -> close());
        HorizontalLayout buttons = new HorizontalLayout(buttonAccept, buttonCancel);
        VerticalLayout layout = new VerticalLayout(lastName, firstName, middleName, specialization, buttons);
        setContent(layout);
    }
}
