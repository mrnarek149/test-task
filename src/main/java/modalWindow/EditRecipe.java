package modalWindow;

import com.vaadin.data.Binder;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import entities.Doctor;
import entities.Patient;
import entities.Priority;
import entities.Recipe;
import service.DoctorService;
import service.PatientService;
import service.RecipeService;

import java.util.List;

/**
 * Формирование формы редактирования
 */
public class EditRecipe extends Edit {
    public EditRecipe(Recipe recipe, Grid grid) {
        init();
        setHeight("620px");
        setWidth("460px");

        PatientService patientService = new PatientService();
        DoctorService doctorService = new DoctorService();
        RecipeService recipeService = new RecipeService();

        Binder<Recipe> binder = new Binder<>(Recipe.class);

        TextArea description = new TextArea();
        description.setValue(recipe.getDescription());
        description.setWordWrap(true);
        description.setSizeFull();
        description.setCaption("Описание");

        ComboBox<Patient> idPatient = new ComboBox<>();
        idPatient.setSizeFull();
        List<Patient> patientList = patientService.getAll();
        idPatient.setItems(patientList);
        idPatient.setEmptySelectionAllowed(false);
        idPatient.setSelectedItem(patientService.findById(recipe.getPatient_id()));
        idPatient.setItemCaptionGenerator(Patient::toString);
        idPatient.setCaption("Выберите пациента");

        ComboBox<Doctor> idDoctor = new ComboBox<>();
        idDoctor.setSizeFull();
        List<Doctor> doctorList = doctorService.getAll();
        idDoctor.setItems(doctorList);
        idDoctor.setEmptySelectionAllowed(false);
        idDoctor.setSelectedItem(doctorService.findById(recipe.getDoctor_id()));
        idDoctor.setItemCaptionGenerator(Doctor::toString);
        idDoctor.setCaption("Выберите доктора");

        DateTimeField dataCreated = new DateTimeField();
        dataCreated.setSizeFull();
        dataCreated.setValue(recipe.getDateCreated());
        dataCreated.setCaption("Дата создания");

        DateTimeField validity = new DateTimeField();
        validity.setValue(recipe.getValidity());
        validity.setCaption("Срок действия");

        ComboBox<Priority> priorityComboBox = new ComboBox<>();
        priorityComboBox.setValue(recipe.getPriority());
        priorityComboBox.setItems(Priority.values());
        priorityComboBox.setEmptySelectionAllowed(false);
        priorityComboBox.setCaption("Приоритет");

        binder.forField(idDoctor).asRequired("Выберите доктора").bind(Recipe::getDoctor, Recipe::setDoctor);
        binder.forField(idPatient).asRequired("Выберите пациента").bind(Recipe::getPatient, Recipe::setPatient);
        binder.forField(description).withValidator(name -> name.length() < 5001, "Ограничения: не более 5000 букв").bind(Recipe::getDescription, Recipe::setDescription);
        binder.forField(dataCreated).asRequired("Укажите дату").bind(Recipe::getDateCreated, Recipe::setDateCreated);
        binder.forField(validity).asRequired("Укажите дату").bind(Recipe::getValidity, Recipe::setValidity);
        binder.forField(priorityComboBox).asRequired("Выберите приоритет").bind(Recipe::getPriority, Recipe::setPriority);

        Button buttonAccept = new Button("ОК");
        buttonAccept.addClickListener(clickEvent -> {
            if (binder.isValid()) {
                recipe.setDescription(description.getValue());
                recipe.setPatient_id(idPatient.getValue().getId());
                recipe.setDoctor_id(idDoctor.getValue().getId());
                recipe.setDateCreated(dataCreated.getValue());
                recipe.setValidity(validity.getValue());
                recipe.setPriority(priorityComboBox.getValue());
                recipeService.update(recipe, recipe.getId());
                grid.getDataProvider().refreshAll();
                close();
            } else {
                buttonAccept.setStyleName(ValoTheme.BUTTON_DANGER);
            }
        });
        Button buttonCancel = new Button("Отменить");
        buttonCancel.addClickListener(clickEvent -> close());
        HorizontalLayout buttons = new HorizontalLayout(buttonAccept, buttonCancel);

        VerticalLayout layout = new VerticalLayout(idPatient, idDoctor, dataCreated, validity, priorityComboBox, description, buttons);
        setContent(layout);
    }
}
