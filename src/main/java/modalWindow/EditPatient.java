package modalWindow;

import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToLongConverter;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import entities.Patient;
import service.PatientService;

/**
 * Формирование формы редактирования
 */
public class EditPatient extends Edit {
    public EditPatient(Patient patient, Grid grid){
        init();
        setHeight("260px");
        setWidth("225px");
        PatientService patientService = new PatientService();

        Binder<Patient> binder = new Binder<>(Patient.class);

        TextField lastName = new TextField();
        lastName.setValue(patient.getLastName());
        lastName.setPlaceholder("Фамилия");

        TextField firstName = new TextField();
        firstName.setValue(patient.getFirstName());
        firstName.setPlaceholder("Имя");

        TextField middleName = new TextField();
        middleName.setValue(patient.getMiddleName());
        middleName.setPlaceholder("Отчество");

        TextField tel = new TextField();
        tel.setValue(String.valueOf(patient.getTel()));
        tel.setPlaceholder("Номер телефона");

        binder.forField(lastName).asRequired("Введите фамилию").withValidator(name -> (name.length() < 16) && (name.matches("[\\p{L}]+")), "Ограничения: не более 15 букв")
                .bind(Patient::getLastName, Patient::setLastName);
        binder.forField(firstName).asRequired("Введите имя").withValidator(name -> (name.length() < 16) && (name.matches("[\\p{L}]+")), "Ограничения: не более 15 букв")
                .bind(Patient::getFirstName, Patient::setFirstName);
        binder.forField(middleName).withValidator(name -> (name.length() < 16) && (name.matches("[\\p{L}| ]+")), "Ограничения: не более 15 букв")
                .bind(Patient::getMiddleName, Patient::setMiddleName);
        binder.forField(tel).asRequired("Введите номер").withValidator(number -> !number.contains(" "), "Содержит пробел")
                .withValidator(number -> number.length() < 14, "Не более 13 цифр").withConverter(new StringToLongConverter("Можно вводить только цифры"))
                .bind(Patient::getTel, Patient::setTel);

        Button buttonAccept = new Button("ОК");
        buttonAccept.addClickListener(clickEvent -> {
            if (middleName.isEmpty()) {
                middleName.setValue(" ");
            }
            if(binder.isValid()){
                patient.setLastName(lastName.getValue());
                patient.setFirstName(firstName.getValue());
                patient.setMiddleName(middleName.getValue());
                patient.setTel(Long.parseLong(tel.getValue()));
                patientService.update(patient, patient.getId());
                grid.getDataProvider().refreshAll();
                close();
            }else{
                buttonAccept.setStyleName(ValoTheme.BUTTON_DANGER);
            }
        });
        Button buttonCancel = new Button("Отменить");
        buttonCancel.addClickListener(clickEvent -> close());
        HorizontalLayout buttons = new HorizontalLayout(buttonAccept, buttonCancel);
        VerticalLayout layout = new VerticalLayout(lastName, firstName, middleName, tel, buttons);
        setContent(layout);
    }
}
