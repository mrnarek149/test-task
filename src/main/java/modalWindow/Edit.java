package modalWindow;

import com.vaadin.ui.Window;

public class Edit extends Window {
    /**
     * Первичная настройка окна
     */
    protected void init(){
        center();
        setModal(true);
        setClosable(false);
        setResizable(false);
    }
}
