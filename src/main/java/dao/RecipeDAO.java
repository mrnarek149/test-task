package dao;

import entities.Recipe;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RecipeDAO implements DAO<Recipe> {
    private DataSource ds;

    public RecipeDAO(DataSource ds){
        this.ds = ds;
    }

    @Override
    public Recipe findById(long id) {
        try (Connection connection = ds.getConnection(); Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM recipe where id = " + id);
            return new Recipe(resultSet.getLong(1), resultSet.getString(2), resultSet.getLong(3),
                              resultSet.getLong(4), resultSet.getTimestamp(5).toLocalDateTime(),
                              resultSet.getTimestamp(6).toLocalDateTime(), resultSet.getString(7));
        } catch (SQLException e) {
            return null;
        }
    }

    public List<Recipe> findByPatient(long id) {
        try (Connection connection = ds.getConnection(); Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM recipe where PATIENT_ID = " + id);
            List<Recipe> recipes = new ArrayList<>();
            while (resultSet.next()) {
                recipes.add(new Recipe(resultSet.getLong(1), resultSet.getString(2), resultSet.getLong(3),
                                       resultSet.getLong(4), resultSet.getTimestamp(5).toLocalDateTime(),
                                       resultSet.getTimestamp(6).toLocalDateTime(),
                                       resultSet.getString(7)));
            }
            return recipes;
        } catch (SQLException e) {
            return null;
        }
    }

    public List<Recipe> findByDoctor(long id) {
        try (Connection connection = ds.getConnection(); Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM recipe where DOCTOR_ID = " + id);
            List<Recipe> recipes = new ArrayList<>();
            while (resultSet.next()) {
                recipes.add(new Recipe(resultSet.getLong(1), resultSet.getString(2), resultSet.getLong(3),
                                       resultSet.getLong(4), resultSet.getTimestamp(5).toLocalDateTime(),
                                       resultSet.getTimestamp(6).toLocalDateTime(), resultSet.getString(7)));
            }
            return recipes;
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public List<Recipe> getAll() {
        try (Connection connection = ds.getConnection(); Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM recipe");
            List<Recipe> list = new ArrayList<>();
            while (resultSet.next()) {
                list.add(new Recipe(resultSet.getLong(1), resultSet.getString(2), resultSet.getLong(3),
                                    resultSet.getLong(4), resultSet.getTimestamp(5).toLocalDateTime(),
                                    resultSet.getTimestamp(6).toLocalDateTime(), resultSet.getString(7)));
            }
            return list;
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public boolean save(Recipe element) {
        try (Connection connection = ds.getConnection(); Statement statement = connection.createStatement()) {
            statement.executeUpdate(String.format("INSERT INTO recipe(DESCRIPTION, PATIENT_ID, DOCTOR_ID, DATA_CREATE, VALIDITY, PRIORITY) VALUES ('%s', '%d', '%d', '"
                                    + Timestamp.valueOf(element.getDateCreated()) + "', '" + Timestamp.valueOf(element.getValidity()) + "', '%s')",
                                    element.getDescription(), element.getPatient_id(), element.getDoctor_id(), element.getPriority().toString()));

            ResultSet id = statement.executeQuery(String.format("SELECT id FROM recipe WHERE description = '%s' AND patient_id = '%d' AND doctor_id = '%d' AND priority = '%s' " +
                                    "AND  data_create = '" + Timestamp.valueOf(element.getDateCreated()) + "' AND validity = '" + Timestamp.valueOf(element.getValidity()) + "'",
                                    element.getDescription(), element.getPatient_id(), element.getDoctor_id(), element.getPriority().toString()));
            while (id.next()) {
                element.setId(id.getLong(1));
            }
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean update(Recipe element, long position) {
        try (Connection connection = ds.getConnection(); Statement statement = connection.createStatement()) {
            statement.executeUpdate(String.format("UPDATE %s SET description = '%s', patient_id = '%d', doctor_id = '%d', data_create = '" +
                            Timestamp.valueOf(element.getDateCreated()) + "', validity = '" + Timestamp.valueOf(element.getValidity()) + "', priority = '%s' WHERE id = %s"
                            , "recipe", element.getDescription(), element.getPatient_id(), element.getDoctor_id(), element.getPriority().toString(), element.getId()));
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    @Override
    public boolean delete(long position) {
        try (Connection connection = ds.getConnection(); Statement statement = connection.createStatement()) {
            statement.executeUpdate("DELETE FROM recipe WHERE id = " + position);
            return true;
        } catch (SQLException e) {
            return false;
        }
    }
}
