package dao;

import java.util.List;

public interface DAO<T> {
    T findById(long id);
    List<T> getAll();
    boolean save(T element);
    boolean update(T element, long position);
    boolean delete(long position);
}
