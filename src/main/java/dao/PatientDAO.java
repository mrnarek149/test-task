package dao;

import entities.Patient;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PatientDAO implements DAO<Patient> {
    private DataSource ds;

    public PatientDAO(DataSource ds){
        this.ds = ds;
    }

    @Override
    public Patient findById(long id) {
        try (Connection connection = ds.getConnection(); Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM patient where id = " + id);
            Patient patient = null;
            while (resultSet.next()){
               patient = new Patient(resultSet.getLong(1), resultSet.getString(2), resultSet.getString(3),
                                     resultSet.getString(4), resultSet.getLong(5));
            }
            return patient;
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public List<Patient> getAll() {
        try (Connection connection = ds.getConnection(); Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM patient");
            List<Patient> list = new ArrayList<>();
            while (resultSet.next()) {
                list.add(new Patient(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3),
                                     resultSet.getString(4), resultSet.getLong(5)));
            }
            return list;
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public boolean save(Patient element) {
        try (Connection connection = ds.getConnection(); Statement statement = connection.createStatement()) {
            statement.executeUpdate(String.format("INSERT INTO patient(last_name, first_name, middle_name, tel) VALUES ('%s','%s','%s','%d')",
                                                  element.getLastName(), element.getFirstName(), element.getMiddleName(), element.getTel()));
            ResultSet id =  statement.executeQuery(String.format("SELECT id FROM patient WHERE last_name = '%s' AND first_name = '%s' AND middle_name = '%s' AND tel = '%d'",
                                                   element.getLastName(), element.getFirstName(), element.getMiddleName(), element.getTel()));
            while (id.next()){
                element.setId(id.getLong(1));
            }
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    @Override
    public boolean update(Patient element, long position) {
        try (Connection connection = ds.getConnection(); Statement statement = connection.createStatement()) {
            statement.executeQuery(String.format("UPDATE %s SET first_name = '%s', middle_name = '%s', last_name = '%s', tel = '%d' WHERE id = %s"
                                                , "patient", element.getFirstName(), element.getMiddleName(), element.getLastName(), element.getTel(), element.getId()));
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    @Override
    public boolean delete(long position) {
        try (Connection connection = ds.getConnection(); Statement statement = connection.createStatement()) {
            statement.executeUpdate("DELETE FROM patient WHERE id = " + position);
            return true;
        } catch (SQLException e) {
            return false;
        }
    }
}
