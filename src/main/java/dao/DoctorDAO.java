package dao;

import entities.Doctor;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DoctorDAO implements DAO<Doctor> {
    private DataSource ds;

    public DoctorDAO(DataSource ds){
        this.ds = ds;
    }

    @Override
    public Doctor findById(long id) {
        try (Connection connection = ds.getConnection(); Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM doctor where id = " + id);
            Doctor doctor = null;
            while (resultSet.next()){
                doctor = new Doctor(resultSet.getLong(1), resultSet.getString(2), resultSet.getString(3),
                                    resultSet.getString(4), resultSet.getString(5));
            }
            return doctor;
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public List<Doctor> getAll() {
        try (Connection connection = ds.getConnection(); Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM doctor");
            List<Doctor> list = new ArrayList<>();
            while (resultSet.next()) {
                list.add(new Doctor(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3),
                                    resultSet.getString(4), resultSet.getString(5)));
            }
            return list;
        } catch (SQLException e) {
            return null;
        }
    }
    @Override
    public boolean save(Doctor element) {
        try (Connection connection = ds.getConnection(); Statement statement = connection.createStatement()) {
            statement.executeUpdate(String.format("INSERT INTO doctor(last_name, first_name, middle_name, specialization) VALUES ('%s','%s','%s','%s')",
                                                  element.getLastName(), element.getFirstName(), element.getMiddleName(), element.getSpecialization()));
            ResultSet id =  statement.executeQuery(String.format("SELECT id FROM doctor WHERE last_name = '%s' AND first_name = '%s' AND middle_name = '%s' AND specialization = '%s'",
                                                                 element.getLastName(), element.getFirstName(), element.getMiddleName(), element.getSpecialization()));
            while (id.next()){
                element.setId(id.getLong(1));
            }
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    @Override
    public boolean update(Doctor element, long position) {
        try (Connection connection = ds.getConnection(); Statement statement = connection.createStatement()) {
            statement.executeUpdate(String.format("UPDATE %s SET first_name = '%s', middle_name = '%s', last_name = '%s', specialization = '%s' WHERE id = %s"
                                                  ,"doctor", element.getFirstName(), element.getMiddleName(), element.getLastName(), element.getSpecialization(), element.getId()));
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    @Override
    public boolean delete(long position) {
        try (Connection connection = ds.getConnection(); Statement statement = connection.createStatement()) {
            statement.executeUpdate("DELETE FROM doctor WHERE id = " + position);
            return true;
        } catch (SQLException e) {
            return false;
        }
    }
}
