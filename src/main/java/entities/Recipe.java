package entities;

import java.time.LocalDateTime;

public class Recipe {
    private long id;
    private String description;
    private long patient_id;
    private long doctor_id;
    private LocalDateTime dateCreated;
    /** Срок действия рецепта*/
    private LocalDateTime validity;
    private Priority priority;
    private Doctor doctor;
    private Patient patient;


    public Recipe(long id, String description, long patient_id, long doctor_id, LocalDateTime dateCreated, LocalDateTime validity, String priority) {
        this.id = id;
        this.description = description;
        this.patient_id = patient_id;
        this.doctor_id = doctor_id;
        this.dateCreated = dateCreated;
        this.validity = validity;
        if (priority.equals(Priority.Нормальный.toString())) {
            this.priority = Priority.Нормальный;
        } else if (priority.equals(Priority.Срочный.toString())) {
            this.priority = Priority.Срочный;
        } else if (priority.equals(Priority.Немедленный.toString())) {
            this.priority = Priority.Немедленный;
        }
    }

    public Recipe(String description, long patient_id, long doctor_id, LocalDateTime dateCreated, LocalDateTime validity, String priority) {
        this.description = description;
        this.patient_id = patient_id;
        this.doctor_id = doctor_id;
        this.dateCreated = dateCreated;
        this.validity = validity;
        if (priority.equals(Priority.Нормальный.toString())) {
            this.priority = Priority.Нормальный;
        } else if (priority.equals(Priority.Срочный.toString())) {
            this.priority = Priority.Срочный;
        } else if (priority.equals(Priority.Немедленный.toString())) {
            this.priority = Priority.Немедленный;
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(long patient_id) {
        this.patient_id = patient_id;
    }

    public long getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(long doctor_id) {
        this.doctor_id = doctor_id;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public LocalDateTime getValidity() {
        return validity;
    }

    public void setValidity(LocalDateTime validity) {
        this.validity = validity;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
