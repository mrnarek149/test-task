package service;

import dao.DoctorDAO;
import entities.Doctor;

import java.util.List;

public class DoctorService extends AbstractService<Doctor> {
    private DoctorDAO doctorDAO = new DoctorDAO(ds);

    @Override
    public Doctor findById(long id) {
        return doctorDAO.findById(id);
    }

    @Override
    public List<Doctor> getAll() {
        return doctorDAO.getAll();
    }

    @Override
    public boolean save(Doctor element) {
        return doctorDAO.save(element);
    }

    @Override
    public boolean update(Doctor element, long id) {
        return doctorDAO.update(element, id);
    }

    @Override
    public boolean delete(long id) {
        return doctorDAO.delete(id);
    }
}
