package service;

import dao.RecipeDAO;
import entities.Recipe;

import java.util.List;

public class RecipeService extends AbstractService<Recipe> {
    private RecipeDAO dao = new RecipeDAO(ds);

    @Override
    public Recipe findById(long id) {
        return dao.findById(id);
    }

    public List<Recipe> findByPatient(long patient_id) {
        return dao.findByPatient(patient_id);
    }

    public List<Recipe> findByDoctor(long doctor_id) {
        return dao.findByDoctor(doctor_id);
    }

    @Override
    public List<Recipe> getAll() {
        return dao.getAll();
    }

    @Override
    public boolean save(Recipe element) {
        return dao.save(element);
    }

    @Override
    public boolean update(Recipe element, long id) {
        return dao.update(element, id);
    }

    @Override
    public boolean delete(long id) {
        return dao.delete(id);
    }
}
