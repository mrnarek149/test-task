package service;

import dao.PatientDAO;
import entities.Patient;

import java.util.List;

public class PatientService extends AbstractService<Patient> {
    private PatientDAO dao = new PatientDAO(ds);

    @Override
    public Patient findById(long id) {
        return dao.findById(id);
    }

    @Override
    public List<Patient> getAll() {
        return dao.getAll();
    }

    @Override
    public boolean save(Patient element) {
        return dao.save(element);
    }

    @Override
    public boolean update(Patient element, long id) {
        return dao.update(element, id);
    }

    @Override
    public boolean delete(long id) {
        return dao.delete(id);
    }
}
