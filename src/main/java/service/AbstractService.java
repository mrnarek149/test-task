package service;

import db.DBConnect;

import javax.sql.DataSource;
import java.util.List;

/**
 * Вспомогающий класс для работы с БД
 * @param <T> Тип сущности (Doctor, Patient, Recipe)
 */
public abstract class AbstractService<T> {
    /** Подключение к БД */
    DataSource ds = DBConnect.getInstance().getDataSource();

    public abstract T findById(long id);

    public abstract List<T> getAll();

    public abstract boolean save(T element);

    public abstract boolean update(T element, long id);

    public abstract boolean delete(long id);
}
